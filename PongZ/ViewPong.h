//
//  ViewPong.h
//  PongZ
//
//  Created by IT-Högskolan on 2015-02-14.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>



int AIpts;
int PlayerPts;
float speed;

NSURL *url;

@interface ViewPong : UIViewController {



    int g;
    NSTimer *timer;
    NSTimer *timer2;
    SystemSoundID PlaySoundID;
    
    AVAudioPlayer *audioPlayer;
}

//    @property (nonatomic) int h;



-(void) BallMovement;
-(void) AIMovement;
-(void) Collision;

@end
